var MOBILE_BREAK_POINT = 768; //px

// Avoid `console` errors in browsers that lack a console.
$(window).load(function(){

    initScrollFade();
    initFixedNav();
    initAnchor();

    var content = $('.content-sections');
    var offset = content.offset().top - $("nav").outerHeight();


    if($(document).scrollTop() >= offset){
        $('html').addClass('scrolled');
    }

    setTimeout(function() {
        if(!$('html').hasClass('scrolled')){
            $("html, body").animate({ scrollTop: offset}, 1000 );
        }
    }, 3000);

    $('main').addClass('loaded');
});

function initAnchor(){

    var links = $('nav a');

    links.click(function(){
        var element = $($(this).attr("href"));

        if(element.length > 0){
            scrollTo(element);
        }

        return false;
    });

    $(window).bind('hashchange', function() {
        var element = $(location.hash);

        if(element.length > 0){
            scrollTo(element);
        }
    });
}

function initScrollFade(){

    $(document).scroll(function() {
        $('html').addClass('scrolled');
        checkFadeIn();
    });

    checkFadeIn();
}

function initFixedNav(){
    var nav = $('nav');
    var offset = nav.offset().top;

    $(document).scroll(function() {
        if($(window).width() > MOBILE_BREAK_POINT && $(document).scrollTop() >= offset) {
            nav.addClass('fixed');
            $('.content-sections').css("margin-top", nav.outerHeight() - 20);
        }
        else{
            nav.removeClass('fixed');
            $('.content-sections').css("margin-top", 0);
        }
    });
}


function checkFadeIn(){
    $('.animate-fade-in').each(function() {
        if(!$(this).hasClass('show') && $(document).scrollTop() + $(window).height() >= getOffset(this).top * 1.025) {
            $(this).addClass('show');
        }
    });
}

function getOffset( el ) {
    var offsetTop = 0, offsetLeft = 0;
    do {
        if ( !isNaN( el.offsetTop ) ) {
            offsetTop += el.offsetTop;
        }
        if ( !isNaN( el.offsetLeft ) ) {
            offsetLeft += el.offsetLeft;
        }
    } while( el = el.offsetParent )

    return {
        top : offsetTop,
        left : offsetLeft
    }
}

function scrollTo(element){
    if($(window).width() > MOBILE_BREAK_POINT){
        $('html, body').animate({
            scrollTop: parseInt(element.offset().top)
        }, "slow", function(){
            window.location.hash = element.attr("id");
        });
    }
    else{
        $('html, body').animate({
            scrollTop: parseInt(element.offset().top)
        }, "slow", function(){
            window.location.hash = element.attr("id");
        });
    }
}

// Place any jQuery/helper plugins in here.
