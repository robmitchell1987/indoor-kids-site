"use strict";

// Load plugins

const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require('gulp-concat');

const jsFiles = [
    "src/bower_components/modernizr/modernizr.js",
    "src/js/lib/conditionizr-4.3.0.min.js",
    "src/bower_components/jquery/dist/jquery.js",
    "src/js/lib/slick.min.js",
    "src/js/lib/snap.svg-min.js",
    "src/js/lib/jquery-ui.min.js",
    "src/js/lib/respimg.min.js",
    "src/js/lib/lazysizes.min.js",
    "src/js/main.js"
];

function css() {
    return gulp
        .src("scss/style.scss")
        .pipe(sass({outputStyle: "compressed"}))
        .pipe(gulp.dest("css"));
}

function js() {
    return gulp
        .src(jsFiles, {allowEmpty: true})
        .pipe(concat("main.min.js"))
        .pipe(gulp.dest("js"));
}

function watchAssets() {
    // You can use a single task
    gulp.watch('./scss/**/*.scss', css);
    // Or a composed task
    // gulp.watch('./js/app.js', js);
}

exports.css = css;
exports.js = js;
exports.default = watchAssets;